# -*- encoding: utf-8 -*-
# stub: capistrano-delayed-job 1.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano-delayed-job".freeze
  s.version = "1.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ruben Stranders".freeze]
  s.date = "2015-02-10"
  s.description = "Capistrano tasks for automatic and sensible DelayedJob configuration.\nEnables zero downtime deployments of Rails applications. Configs can be\ncopied to the application using generators and easily customized.\nWorks *only* with Capistrano 3+.\nInspired by https://github.com/bruno-/capistrano-unicorn-nginx and http://bl.ocks.org/dv/10370719\n".freeze
  s.email = ["r.stranders@gmail.com".freeze]
  s.homepage = "https://github.com/capistrano-plugins/capistrano-delayed-job".freeze
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Capistrano tasks for automatic and sensible DelayedJob configuration.".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<capistrano>.freeze, [">= 3.1"])
      s.add_runtime_dependency(%q<sshkit>.freeze, [">= 1.2.0"])
      s.add_runtime_dependency(%q<daemons>.freeze, [">= 1.1"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
    else
      s.add_dependency(%q<capistrano>.freeze, [">= 3.1"])
      s.add_dependency(%q<sshkit>.freeze, [">= 1.2.0"])
      s.add_dependency(%q<daemons>.freeze, [">= 1.1"])
      s.add_dependency(%q<rake>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<capistrano>.freeze, [">= 3.1"])
    s.add_dependency(%q<sshkit>.freeze, [">= 1.2.0"])
    s.add_dependency(%q<daemons>.freeze, [">= 1.1"])
    s.add_dependency(%q<rake>.freeze, [">= 0"])
  end
end
