# -*- encoding: utf-8 -*-
# stub: capistrano_colors 0.5.5 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano_colors".freeze
  s.version = "0.5.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.2".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Mathias Stjernstrom".freeze]
  s.date = "2011-03-17"
  s.description = "Simple gem to display colors in capistrano output.".freeze
  s.email = "mathias@globalinn.com".freeze
  s.extra_rdoc_files = ["README.rdoc".freeze, "lib/capistrano_colors.rb".freeze, "lib/capistrano_colors/configuration.rb".freeze, "lib/capistrano_colors/logger.rb".freeze]
  s.files = ["README.rdoc".freeze, "lib/capistrano_colors.rb".freeze, "lib/capistrano_colors/configuration.rb".freeze, "lib/capistrano_colors/logger.rb".freeze]
  s.homepage = "http://github.com/stjernstrom/capistrano_colors".freeze
  s.rdoc_options = ["--line-numbers".freeze, "--inline-source".freeze, "--title".freeze, "Capistrano_colors".freeze, "--main".freeze, "README.rdoc".freeze]
  s.rubyforge_project = "capistranocolor".freeze
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Simple gem to display colors in capistrano output.".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<capistrano>.freeze, [">= 2.3.0"])
    else
      s.add_dependency(%q<capistrano>.freeze, [">= 2.3.0"])
    end
  else
    s.add_dependency(%q<capistrano>.freeze, [">= 2.3.0"])
  end
end
