class Plugins::FacebookSn::FacebookSn < ActiveRecord::Base
  belongs_to :site, class_name: "CamleonCms::Site", dependent: :destroy

  def self.is_page_id_duplicated?(page_id)
  	self.find_by_fb_page_id(page_id).present?
  end

end