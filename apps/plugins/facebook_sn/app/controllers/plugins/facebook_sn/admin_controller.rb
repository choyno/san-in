class Plugins::FacebookSn::AdminController < CamaleonCms::Apps::PluginsAdminController
  include Plugins::FacebookSn::MainHelper
  def index
  end

  # show settings form
  def settings
    @fb_pages = current_site.myplugin.all
  end

  # save values from settings form
  def save_settings
    if  params[:fb_page_name].present?
        params[:fb_page_id].present? &&
        params[:fb_page_token].present?
      if !current_site.myplugin.is_page_id_duplicated?(params[:fb_page_id])
        fb_page = current_site.myplugin.new(
          fb_page_name: params[:fb_page_name], 
          fb_page_id: params[:fb_page_id], 
          fb_page_token: params[:fb_page_token])
        if fb_page.save!
          redirect_to url_for(action: :settings), notice: 'FB Page credentials saved successfully'
        else
          redirect_to url_for(action: :settings), error: "FB Page credentials unsuccessfully saved"
        end
      else
        redirect_to url_for(action: :settings), error: "Page ID must be unique."
      end
    else
      redirect_to url_for(action: :settings), error: 'All fields must be filled!'
    end
  end

  def edit
    @fb_page = current_site.myplugin.find(params[:id])
  end

  def update
    fb_page_id = current_site.myplugin.find(params[:id])
    if params[:fb_page_name].present?
        params[:fb_page_id].present? &&
        params[:fb_page_token].present?

        fb_page = fb_page_id.update(
          fb_page_name: params[:fb_page_name], 
          fb_page_id: params[:fb_page_id], 
          fb_page_token: params[:fb_page_token])

        if fb_page
          redirect_to url_for(action: :settings), notice: 'FB Page credentials updated successfully'
        else
          redirect_to url_for(action: :settings), error: "FB Page credentials unsuccessfully updated"
        end
    else
      redirect_to url_for(action: :edit), error: "Page ID must be unique."
    end
  end

  def destroy
    fb_page = current_site.myplugin.find(params[:id])
    if fb_page.delete
      redirect_to url_for(action: :settings), notice: 'FB Page credentials removed successfully'
    else
      redirect_to url_for(action: :settings), error: 'FB Page credentials unsuccessfully removed'
    end
  end
end
