module Plugins::FacebookSn::MainHelper
  def self.included(klass)
    # klass.helper_method [:my_helper_method] rescue "" # here your methods accessible from views
  end

  # here all actions on going to active
  # you can run sql commands like this:
  # results = ActiveRecord::Base.connection.execute(query);
  # plugin: plugin model
  def facebook_sn_on_active(plugin)
    unless ActiveRecord::Base.connection.table_exists? 'plugins_facebook_sns'
      ActiveRecord::Base.connection.create_table :plugins_facebook_sns do |t|
        t.integer :site_id
        t.string :fb_page_name
        t.string :fb_page_id
        t.string :fb_page_token
        t.timestamps
      end
    end

    CamaleonCms::Site.class_eval do
      has_many :facebook_sns, class_name: "Plugins::FacebookSns::Models::FacebookSns"
    end

    fb_plugin = current_site.plugins.where(name: 'facebook_sn')
    if fb_plugin[0]['status'].nil? || fb_plugin[0]['status'] == '0'
      fb_plugin.update(status: '1')
    end

    unless ActiveRecord::Base.connection.column_exists?(:plugins_facebook_sns, :status_message)
      ActiveRecord::Base.connection.add_column(:plugins_facebook_sns, :status_message, :string)
    end

  end

  # here all actions on going to inactive
  # plugin: plugin model
  def facebook_sn_on_inactive(plugin)
    fb_plugin = current_site.plugins.where(name: 'facebook_sn')
    if fb_plugin[0]['status'] == '1'
      fb_plugin.update(status: '0')
    end
  end

  # here all actions to upgrade for a new version
  # plugin: plugin model
  def facebook_sn_on_upgrade(plugin)
  end

  # hook listener to add settings link below the title of current plugin (if it is installed)
  # args: {plugin (Hash), links (Array)}
  # permit to add unlimmited of links...
  def facebook_sn_on_plugin_options(args)
    args[:links] << link_to('Settings', admin_plugins_facebook_sn_settings_path)
  end

  def facebook_sn_admin_before_load
    admin_menu_add_menu('custom_last_menu', {icon: 'fa fa-facebook', title: 'Facebook SNS', url: admin_plugins_facebook_sn_settings_path})
  end

end
