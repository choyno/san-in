$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "facebook_sn/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "facebook_sn"
  s.version     = FacebookSn::VERSION
  s.authors     = ["Michael Angelo P. Larrubis"]
  s.email       = ["michaellarrubis@gmail.com"]
  s.homepage    = ""
  s.summary     = ": Summary of FacebookSn."
  s.description = ": Description of FacebookSn."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "camaleon_cms", "~> 2.0"

  s.add_development_dependency "sqlite3"
end
