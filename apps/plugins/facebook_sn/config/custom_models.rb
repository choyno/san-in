Rails.application.config.to_prepare do
  CamaleonCms::Site.class_eval do
    has_many :myplugin, class_name: "Plugins::FacebookSn::FacebookSn", dependent: :destroy
  end
end
