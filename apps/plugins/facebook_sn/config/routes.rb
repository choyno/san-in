require 'plugin_routes'
require 'camaleon_cms/engine'
Rails.application.routes.draw do
  scope PluginRoutes.system_info["relative_url_root"] do
    scope '(:locale)', locale: /#{PluginRoutes.all_locales}/, :defaults => {  } do
      # frontend
      namespace :plugins do
        namespace 'facebook_sn' do
          get 'index' => 'front#index'
        end
      end
    end

    #Admin Panel
    scope :admin, as: 'admin', path: PluginRoutes.system_info['admin_path_name'] do
      namespace 'plugins' do
        namespace 'facebook_sn' do
          controller :admin do
            get   :index
            get   :settings
            get  "/settings/:id/edit" => :edit
            post "/settings/:id/edit" => :update
            post "settings"           => :save_settings
            get "/settings/:id"       => :destroy
            delete "/settings/:id"    => :destroy
          end
        end
      end
    end

    # main routes
    #scope 'facebook_sn', module: 'plugins/facebook_sn/', as: 'facebook_sn' do
    #  Here my routes for main routes
    #end
  end
end
