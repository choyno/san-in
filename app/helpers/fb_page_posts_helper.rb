module FbPagePostsHelper

	def get_page_posts

		check_fb_plugin_installed = ActiveRecord::Base.connection.data_source_exists? 'plugins_facebook_sns'
		fb_plugin = current_site.plugins.where(slug: 'facebook_sn')
		status = fb_plugin[0]['status'].to_i

		return false if !check_fb_plugin_installed
		return false if !fb_plugin.present?
		return false if status == 0

		fb_pages = current_site.myplugin.all
		fetched_page_posts = []
		fb_pages.each do |fb_page|
			parsed_data = JSON.parse(
				Net::HTTP.get(URI(
						"https://graph.facebook.com/v3.2/#{fb_page['fb_page_id']}?
							fields=
								name,
								picture,
								feed.limit(10){
									created_time,
									attachments,
									updated_time,
									message,
									description,
									full_picture,
									name,
									permalink_url,
									status_type}
								&access_token=#{fb_page['fb_page_token'].gsub(/"|\[|\]/, '')}"
					))
				)
			if parsed_data['error'].present?
				update_status_message(fb_page['fb_page_id'], parsed_data['error']['message'])
			else
				if ActiveRecord::Base.connection.column_exists?(:plugins_facebook_sns, :status_message)
					update_status_message(fb_page['fb_page_id'], "Page request is okay.")
					fetched_page_posts << parsed_data
				else 
					fetched_page_posts << parsed_data
				end
			end
		end
		all_page_posts = []
		fetched_page_posts.each do |post|
			if post['feed'].present?
				post['feed']['data'].each do |post_item|
					page_posts_item = {
						'page_name' 					=> post['name'],
						'page_picture_url' 		=> post['picture']['data']['url'],
						'post_name' 					=> post_item['name'],	
						'post_permalink_url' 	=> post_item['permalink_url'],	
						'post_status_type' 		=> post_item['status_type'],
						'post_attachments'		=> post_item['attachments'],
						'post_description' 		=> post_item['description'],	
						'post_created' 				=> post_item['created_time'],	
						'post_message' 				=> post_item['message'],	
						'post_full_picture' 	=> post_item['full_picture'],	
					}
					all_page_posts << page_posts_item
				end
			end
		end

		return all_page_posts.sort_by {|post| post['post_created']}.reverse

	end

	def update_status_message(fb_page_id, status_message)
		fb_page = current_site.myplugin.where(fb_page_id: fb_page_id)
		fb_page.update(status_message: status_message)
	end

  def break_the_lines(text)
    text.to_s.gsub(/\n/, '<br/>').html_safe
  end

  def check_for_hashtag(text)
  	text = text.html_safe
  	split_text = text.split(" ")
  	new_text = []
  	split_text.each do |result|
  		if result.start_with? '#'
  			new_text << "<span class='sns-hash-tag'>#{result}</span>"
  		else
  			new_text << result
  		end
  	end
  	return new_text.join(" ")
  end

  def check_status_type(status_type, has_shared, shared_post_owner, has_attached_img)
  	case status_type
  	when "added_photos"
  		if has_attached_img
  			if has_attached_img > 1
  				return "added <span class='sns-post-owner'>#{has_attached_img}</span> photos".html_safe
  			else
  				return "added a photo"
  			end
  		else
  			return "added a photo"
  		end
  	when "mobile_status_update"
  		if has_shared
  			return "shared <span class='sns-post-owner'>#{shared_post_owner}'s</span> post".html_safe
  		else
  			return "added a post"
  		end
  	when "added_video"
  		return "added a video"
  	else
  		return "added a post"
  	end
  end

end
