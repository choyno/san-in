class WelcomeController < ApplicationController
  def index
  end

  # Temp Pages
  def special_article
  end

  def article
  end

  def article_index
  end
end
