$(document).ready ->
  winWidth = $(window).width()
  langHeight = $('.js-language-menu-heading').outerHeight()
  accessTrain = $('#article-access-content1').children().length
  accessAirplane = $('#article-access-content2').children().length
  accessBus = $('#article-access-content3').children().length

  $('.carousel').slick
    centerMode: true
    centerPadding: 0
    infinite: true
    slidesToShow: 1
    slidesToScroll: 1
    variableWidth: true
    reponsive: [ {
      breakpoint: 767
      settings: variableWidth: true
    } ]

  $('.js-accordion-heading').click ->
    $(this).toggleClass 'is-open'
    $(this).next('.accordion-content').slideToggle()

  $('.js-header-humberger-list').click ->
    $('body, html').toggleClass 'no-scroll'
    $('.header-container').toggleClass 'nav-open'
    $('.language-menu-trigger').prop 'checked', false

  

  $(window).resize ->
    $('body, html').removeClass 'no-scroll'
    $('.header-container').removeClass 'nav-open'
    $('.language-menu-trigger').prop 'checked', false

  $('.js-access-anchor-text').click ->
    $('.access-anchor-wrapper').toggleClass 'is-open'

  $('.access-anchor-item').click ->
    $('.access-anchor-wrapper').removeClass 'is-open'
    $('.access-anchor-text-inner').text $(this).children('.access-anchor-link').text()

  if winWidth < 768
    $('.js-footer-menu-contents').click ->
      $(this).next("ul").toggle(300)
      $(this).toggleClass('is-open')

    $('.nav-header-link').click ->
      $(this).parent().toggleClass 'is-open'

    $('.submenu-link').click ->
      $(this).parent().toggleClass 'is-open-inner'

  if $('.submenu-list').children('.submenu-inner').length > 0
    $('.submenu-list').children('.submenu-link').addClass 'has-caret'

  $('.article-content-text p:has(img)').addClass 'article-has-img'
  $('.article-content-text p').find('img').wrap '<span class="article-content-photo"><span class="article-content-photo-inner"></span></span>'

  if accessTrain > 1
    $('#article-access-content1').addClass 'access-transpo-icon'

  if accessAirplane > 1
    $('#article-access-content2').addClass 'access-transpo-icon'

  if accessBus > 1
    $('#article-access-content3').addClass 'access-transpo-icon'

  # SNS Feature
  $('#sns-carousel').slick
    centerMode: true
    infinite: true
    slidesToShow: 4
    variableWidth: true
    centerPadding: '60px'
    mobileFirst: true
    reponsive: [
      {
        breakpoint: 768
        settings:
          variableWidth: true
          slidesToShow: 3
          slidesToScroll: 1
      }
    ]

  $('a[href*=\\#]').on 'click', (event) ->
    event.preventDefault()
    $('html,body').animate { scrollTop: $(@hash).offset().top }, 500
