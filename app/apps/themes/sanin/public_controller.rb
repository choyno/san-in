class Themes::Sanin::PublicController < CamaleonCms::FrontendController

  def home
    @seo = {
      title: "SAN'IN -Tottori and Shimane",
      description: "SAN’IN is a tourist information site introducing Japanese history, culture, landscape roots. You can discover and discover SAN 'IN not in other areas.",
      keywords: "San'in",
    }
  end

  def travel_brochures
    @seo = {
      title: "SAN'IN -Tottori and Shimane",
      description: "SAN’IN is a tourist information site introducing Japanese history, culture, landscape roots. You can discover and discover SAN 'IN not in other areas.",
      keywords: "San'in",
    }
  end

  def about_san_in
    @seo = {
      title: "SAN'IN About | SAN'IN -Tottori and Shimane",
      description: "Many of the quintessential parts of Japanese culture are left in San-in area. The history, culture, life, and the view of nature here all lead to the roots of Japan.",
      keywords: "San'in",
    }
  end

  def accommodation
    @seo = {
      title: "Accommodation that you would like to visit | SAN'IN -Tottori and Shimane",
      description: "Accommodation that you would like to visit when traveling to the Sanin.",
      keywords: ""
    }
  end

  def discover
  end

  def privacy_policy
    @seo = {
      title: "Privacy policy | SAN'IN -Tottori and Shimane",
      description: "This is the privacy policy of SANIN website. We understand the importance of protecting your information.",
      keywords: ""
    }
  end

  def access
    @seo = {
      title: "Access and Travel Tips for San'in,Japan | SAN'IN -Tottori and Shimane",
      description: "Access and Travel Tips for San'in, Japan",
      keywords: ""
    }
  end

  def destinations
    @seo = {
      title: "SAN'IN Destinations | SAN'IN -Tottori and Shimane",
      description: "We introduce the charm of San'in by each area of East, Central and West. You can see by narrowing down from maps and areas.",
      keywords: ""
    }
    @area = params[:area].presence || 'east'
    @spots = current_site.the_posts('featured-list').joins(:custom_field_values).where("custom_field_slug = 'area-pick-up-spots' AND value = '#{@area}'").decorate

    locations = []
    @spots.each do |spot|
      locations.push([spot.the_fields('location-name-pick-up-spots')[0], spot.the_fields('latitude-pick-up-spots')[0], spot.the_fields('longitude-pick-up-spots')[0]])
    end
    gon.locations = locations

  end

  def tourist_information
    @seo = {
      title: "Tourist Information Center in each spot in Sanin | SAN'IN -Tottori and Shimane",
      description: "Tourist information office in each spot in Sanin",
      keywords: ""
    }
  end

  def highlights
  end

  def stay
  end

  def contact_us
    @seo = {
      title: "Inquiry and questions | SAN'IN -Tottori and Shimane",
      description: "Inquiry form where you can view tips on using the service and answers to frequently asked questions",
      keywords: ""
    }
  end

  def featured
    redirect_to "/#{pget_lang}/featured-list"
  end

  def news
    redirect_to "/#{pget_lang}/news-list"
  end

  def featuredlist
    redirect_to "/#{pget_lang}/featured-list"
  end

  def newslist
    redirect_to "/#{pget_lang}{/news-list"
  end

  def first_timers
    @seo = {title: "SAN’IN For First-Timers | SAN’IN -tottori and shimane-"}
    @post_type =  current_site.post_types.find_by_slug("news-list").decorate
  end

  def pget_lang
    I18n.locale.to_s
  end

end
