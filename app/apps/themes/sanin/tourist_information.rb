module Themes::Sanin::TouristInformation

  def self.included(klass)
     klass.helper_method [:tourist_information_details] rescue "" # here your methods accessible from views
  end

  def tourist_information_details
    [

      {"airport": "Tottori Sand Dunes Conan Airport",
       schedules: [
           {
             ITC: "Tottori City International Tourist Support Center",  latitude:"35.4937646", longitude:"134.2246907", address:"117 Higashihonjicho Town, Tottori City, Tottori Pref. 680-0835", tel:"0857-22-7935", time:"8:30 AM - 5:30 PM (All year exept 31th December and 1st January)", mail:"international@hal.ne.jp", web:"https://www.city.tottori.lg.jp/www/contents/1336986251261/index_k.html", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1246",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Part-Time)"}, {name: "Korean(Part-Time)"} ], wifi: true, internet_pc: true,
             related_contents: [{image: "", name: "鳥取砂丘砂の美術館", extra: "E-3" }, {image: "", name: "石谷家住宅", extra: "E-4" }]
           },
           {
             ITC: "Kurayoshi Shirakabe-dozo Tourist Information Center",   latitude:"35.43134", longitude:"133.8226033 ", address:"2568 Uomachi Town, Kurayoshi City, Tottori Pref. 682-0821 (in Akagawara-jyugokan)", tel:"0858-22-1200", time:"8:30 AM - 5:00 PM (10:00 AM - 4:00 PM from 31 Dec. to 3 Jan.)", mail:"info@kurayoshi-kanou.jp", web:"https://www.kurayoshi-kankou.jp/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3321",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Full-Time)"}, {name: "Korean(Full-Time)"} ], wifi: true, internet_pc: false,
             related_contents: [{image: "", name: "赤瓦・白壁土蔵群", extra:"E-6" }, {image: "", name: "はわい温泉", extra: "E-7" }]
           },
           {
             ITC: "Central Tottori International Tourist Support Center", latitude:"35.4544035",  longitude:"133.8468281", address:"2F 2-1-2 Ageicho Town, Kurayoshi City, Tottori Pref. 682-0022", tel:"0858-24-5024", time:"8:30 AM - 5:30 PM (except 31 December and 1 January)", mail:"", web:"https://tottori-iyashitabi.com/en/profile/476/", web_JNTO:"",
             languages: [  ], wifi:false, internet_pc:false,
             related_contents: [{image: "", name: "三徳山", extra:"E-7" }, {image: "", name: "三朝温泉", extra: "E-7" }]
           }
        ]
      },

      {"airport": "Yonago Kitaro Airport ",
         schedules: [
           {
             ITC: "Sakaiminato City Tourist Information Center", latitude:"35.5325747",  longitude:"133.2234827 ", address:"215 Taishomachi Town, Sakaiminato City, Tottori Pref. 684-0004", tel:"0859-47-0121", time:"9:00 AM - 4:00 PM Mar. - Oct. 9:00 AM - 5:00 PM Nor. - Feb.", mail:"info@sakaiminato.net", web:"http://www.sakaiminato.net/foreign/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1248",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Full-Time)"}, {name: "Korean(Part-Time)"} ], wifi:true, internet_pc:false,
             related_contents: [{image: "", name: "隠岐　牛突き", extra:"E-17" }, {image: "", name: "皆生温泉", extra: "E-19" }]
           },
           {
             ITC: "Yonago Airport General Information", latitude:"35.5006273",  longitude:"133.2429285 ", address:"1634 Sainokamicho Town, Sakaiminato City, Tottori Pref. 684-0055", tel:"0859-45-6123", time:"8:00 AM - Last arrival flights.", mail:"contact@yonago-air.com", web:"http://www.yonago-air.com/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3213",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Part-Time)"}, {name: "Korean(Part-Time)"} ], wifi:true, internet_pc:false,
             related_contents: [{image: "", name: "美保神社", extra:"E-11" }, {image: "", name: "大山", extra: "E-14" }]
           },
           {
             ITC: "Adachi Museum of Art International Tourist Information Desk",  latitude:"35.3802986", longitude:"133.1942248", address:"320 Furukawacho Town, Yasugi City, Shimane Pref. 692-0064", tel:"0854-28-7111", time:"9:00 AM - 5:30 PM Apr. - Sep. 9:00 AM - 5:00 PM Oct. - Mar. (Open year round)", mail:"info@adachi-museum.or.jp", web:"http://www.adachi-museum.or.jp/en/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1250",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Part-Time)"} ], wifi:true, internet_pc:false,
             related_contents: [{image: "", name: "足立美術館", extra:"E-16" }, {image: "", name: "皆生温泉", extra: "E-19" }]
           },
           {
             ITC: "Yasugi-city Tourist Information Center",  latitude:"35.4298252", longitude:"133.26", address:"2093-3 Yasugicho Town, Yasugi City, Shimane Prefecture 692-0011", tel:"0854-23-7667", time:"7:30 AM - 7:00 PM (All year exept 31th December and 1st January)", mail:"mail@yasugi-kankou.jp", web:"http://www.yasugi-kankou.com/eng/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3297",
             languages: [ {name: "English(Full-Time)"} ], wifi:false, internet_pc:false,
             related_contents: [{image: "", name: "足立美術館", extra:"E-16" }, {image: "", name: "皆生温泉", extra: "E-19" }]
           }
        ]
      },

      {"airport": "Izumo Enmusubi Airport",
         schedules: [
           {
             ITC: "Izumo Airport Information Counter",  latitude:"35.4147765", longitude:"132.8834916", address:"2633-1 Okinosu, Hikawacho Town, Izumo City, Shimane Pref. 699-0551", tel:"0853-72-7500", time:"8:00 AM - 8:30 PM", mail:"", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3245",
             languages: [ {name: "English(Full-Time)"} ], wifi:true, internet_pc:false,
             related_contents: [{image: "", name: "玉造温泉", extra:"E-19" }, {image: "", name: "石見銀山", extra: "E-21" }]
           },
           {
             ITC: "Izumoshi Station Tourist Information Center",  latitude:"35.3606911", longitude:"132.7542417", address:"11 Ekikitamachi Town, Izumo City, Shimane Pref. 693-0007 (in JR Izumoshi Station)", tel:"0853-30-6015", time:"8:30 AM - 5:00 PM", mail:"", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3235",
             languages: [ {name: "English(Full-Time)"} ], wifi:false, internet_pc: false ,
             related_contents: [{image: "", name: "鉄の歴史博物館", extra:"E-13" }, {image: "", name: "石見銀山", extra: "E-21" }]
           },
           {
             ITC: "Shinmondori Visitor Center",  latitude:"35.3948163", longitude:"132.6846487", address:"780-4 kizukiminami, Taishamachi Town, Masuda City, Shimane Pref. 699-0711", tel:"0853-53-2298", time:"9:00 AM - 5:00 PM", mail:"taisha@kankou-taisha.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3168",
             languages: [ {name: "English(Full-Time)"}], wifi: true, internet_pc: false,
             related_contents: [{image: "", name: "出雲大社", extra:"E-9" }, {image: "", name: "湯の川温泉", extra: "E-19" }]
           },
           {
             ITC: "Matsue International Tourist Information Center",  latitude:"35.46431", longitude:"133.0613043", address:"665 Asahimachi Town, Matsue City, Shimane Pref. 690-0003 (in JR Matsue Station)", tel:"0852-21-4034", time:"9:00 AM - 6:00 PM (9:00 AM - 7:00 PM Jun.-Oct. Open year round)", mail:"info@kankou-matsue.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1249",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Part-Time)"}, {name: "Korean(Part-Time)"} ], wifi:true, internet_pc:true,
             related_contents: [{image: "", name: "松江城", extra:"E-10" }, {image: "", name: "一畑薬師", extra: "E-12" }]
           },
           {
             ITC: "Matsue Urban Hotel",  latitude:"35.4653214", longitude:"133.0625516", address:"590-3 Asahimachi Town, Matsue City, Shimane Pref. 690-0003", tel:"0852-22-0002", time:"24H", mail:"", web:"http://station.matsue-urban.co.jp/en/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3221",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Part-Time)"} ], wifi: true, internet_pc: true,
             related_contents: [{image: "", name: "堀川遊覧船", extra:"E-10" }, {image: "", name: "佐香神社（日本酒発祥の地）", extra: "E-15" }]
           },
           {
             ITC: "Matsue New Urban Hotel",  latitude:"35.468822", longitude:"133.0488213", address:"40-1 Nishichamachi Town, Matsue City, Shimane Pref. 690-0845", tel:"0852-23-0003", time:"24H", mail:"new-urban@matsue-urban.co.jp", web:"http://new.matsue-urban.co.jp/en", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3217",
             languages: [ {name: "English(Full-Time)"}], wifi: true, internet_pc: true,
             related_contents: [{image: "", name: "堀川遊覧船", extra:"E-10" }, {image: "", name: "松江しんじ湖温泉", extra: "E-19" }]
           }
        ]
      },

      {"airport": "Hagi Iwami Airport",
         schedules: [
           {
             ITC: "Masuda City Tourist Information Center (Masuda Tourist Association)",  latitude:"34.6725641", longitude:"131.8359542", address:"17-2 Ekimaecho Town, Masuda City, Shimane Pref. 698-0024", tel:"0856-22-7120", time:"9:00 AM - 5:30 PM (All year exept 29th December and 3rd January)", mail:"info2@masudashi.com", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1406",
             languages: [ {name: "English(Full-Time)"} ], wifi: true, internet_pc: true,
             related_contents: [{image: "", name: "石見神楽", extra:"E-20" }, {image: "", name: "温泉津温泉", extra: "E-26" }]
           },
           {
             ITC: "Tsuwano International Information Center (Tsuwano Tourism Association)",  latitude:"34.4724264", longitude:"131.7719608", address:"I71-2 Ushiroda, Tsuwano Town, Kanoashi District, Shimane Pref. 699-5605", tel:"0856-72-1771", time:"9:00 AM - 5:00 PM", mail:"tsuwanok@tsuwano.net", web:"http://www.tsuwano.ne.jp/kanko/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1405",
             languages: [ {name: "English(Full-Time)"}], wifi: true, internet_pc: false,
             related_contents: [{image: "", name: "殿町通り", extra:"E-23" }, {image: "", name: "太鼓谷稲成神社", extra: "E-22" }]
           },
           {
             ITC: "Hagi Hotel & Restaurant Takadai",  latitude:"34.4102145", longitude:"131.3978448", address:"80 Karahimachi Town, Hagi City, Yamaguchi Pref. 758-0044", tel:"0838-22-0065", time:"9:00 AM - 9:00 PM", mail:"takadai@takadai.co.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1266",
             languages: [ {name: "English(Full-Time)"},{ name: "Chinese(Full-Time)"} ], wifi: true, internet_pc: true,
             related_contents: [{image: "", name: "太鼓谷稲成神社", extra:"E-22" }, {image: "", name: "萩の街並み", extra: "E-27" }]
           },
           {
             ITC: "Hagi City Tourism Assciation",  latitude:"34.3939883", longitude:"131.3985912", address:"3537-3 Tsubaki, Hagi City, Yamaguchi Pref. 758-0061", tel:"0838-25-1750", time:"9:00 AM - 5:45 PM Mar. - Nov. 9:00 AM - 5:00 PM Dec. - Feb.", mail:"info@hagishi.com", web:"http://www.hagishi.com/en/", web_JNTO:"http://www.hagishi.com/en/",
             languages: [ {name: "English(Full-Time)"} ], wifi: true, internet_pc: false,
             related_contents: [{image: "", name: "太鼓谷稲成神社", extra:"E-22" }, {image: "", name: "萩の街並み", extra: "E-27" }]
           }
        ]
      }

     #{"airport": "Hagi Iwami Airport",
     #   schedules: [
     #     {
     #       ITC: "",  latitude:"", longitude:"", address:"", tel:"", time:"", mail:"", web:"", web_JNTO:"",
     #       languages: [ {name: "English", status: "Full-Time"},{ name: "Chinese", status: "Part-Time"}, {name: "Korean", status: "Part-Time"} ], wifi: true, internet_pc: true,
     #       related_contents: [{image: "", name: "", extra:"" }, {image: "", name: "", extra: "" }]
     #     }
     #  ]
     #},

    ]
  end

end
