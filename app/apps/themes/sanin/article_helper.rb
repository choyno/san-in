module Themes::Sanin::ArticleHelper

  def self.included(klass)
     klass.helper_method [:translate_word, :get_post_detail, :get_posts_by_category, :is_selected, :ymd_format, :get_category_id, :current_tab, :special_layout_num, :use_big_layout] rescue "" # here your methods accessible from views
  end

  def get_posts_by_category(params_categories)
    post_ids =  []
    category_ids = params_categories.flatten.to_i
    current_site.the_full_categories.where(id: category_ids).decorate.each do |category|
      post_ids = post_ids <<  category.the_posts.pluck(:id).flatten.to_i
    end

    return post_ids.flatten
  end

  def is_selected( categories, check_value )
     status =  false
     f_categories = categories.flatten.to_i
     f_categories.include?(check_value)
  end


  def current_tab( tab_name, current_tab)
      current_tab == tab_name
  end

  def ymd_format(post_date)
    (post_date ).strftime("%Y. %m. %d")
  end

  def my_content_shortcodes
    shortcode_add("content_image", theme_view('partials/article/shortcodes/content_image'), 'Render Content Image, sample: [content_image images="domain.com/image1.png,  domaain.com/image2.png"]')
    shortcode_add("header_banner_title", theme_view('partials/article/shortcodes/header_banner_title'), 'Render Header Banner title, sample: [header_banner_title title_1="" title_2="" title_3="" title_4=""]')
  end

  def get_category_id(post_type, slug)
    cat = post_type.categories.find_by(slug: slug)
    return (cat.id if cat)
  end

  def get_post_detail(id)
    post = { name: "", url: "", image:"" }
    get_post =  current_site.the_posts('featured-list').where(id: id).first
    if get_post.present?
      dget_post = get_post.decorate
      img_path = dget_post.the_fields('header-banner-image')[0]
      banner_title = dget_post.the_fields('banner-title')[0]
      name =  banner_title.present? ? banner_title : dget_post.the_title

      url =  dget_post.the_slug

      post = { name: name, url: "/#{get_lang}/featured/#{url}", image: img_path}
    end

    return post
  end

 def special_layout_num(num = 10)
   list = []
   (0..num).each_slice(3){ |slice| list << ( p slice[0]) }
   return list
 end

 def use_big_layout(list_idx, current_idx)
   return "special-article-big" if list_idx.include?(current_idx)
 end

 def translate_word(word)
   (word).to_s.translate(get_lang)
 end

end
