module Themes::Sanin::MainHelper
  def self.included(klass)
    klass.helper_method [:get_image_path, :lazy_image_tag, :get_lang, :locale_path, :custom_draw_languages, :cama_url_to_fixed] rescue "" # here your methods accessible from views
  end

  def sanin_settings(theme)
    # callback to save custom values of fields added in my_theme/views/admin/settings.html.erb
  end

  # callback called after theme installed
  def sanin_on_install_theme(theme)

      #return if theme.get_option('installed_at').present?

      basic_data_options =  {has_category: false, has_tags: false, not_deleted: true, has_summary: false, has_content: true,
                             has_comments: false, has_picture: true, has_template: false, has_layout: false, has_seo: false}

      current_site.post_types.create(name: "Page", slug: "page", data_options: basic_data_options)
      pt = current_site.the_post_type('page')

      #Default Page
      #pt.add_post(title: 'Contact Us', slug:"contact-us", content: '<p></p>', settings:{ defaut_layout: "index"}) if !pt.the_post("contact-us").present?

      if current_site.the_post_type("post").present?
        #overide the default post content group to special
        post_group = current_site.the_post_type("post")
        post_group.update(name: "Special Article",description: "Main/ Special article",  slug: "special", data_options: basic_data_options)
      end

      if !current_site.the_post_type("special").present?
        group_special = current_site.post_types.create(name: "Special Article", slug: "special", data_options: basic_data_options)
      else
        group_special = current_site.the_post_type("special")
      end

      #Special Article
      if current_site.the_post_type("special").present?

        special_banner_image = group_special.add_field_group({name: "Header Banner Image", slug: "_group-special-header-banner-image", is_repeat: false })
        special_banner_image.add_field({"name"=>"Header Banner Image", "slug"=>"header-banner-image"},{ field_key: "image", multiple: false, required: true})
        special_banner_image.add_field({"name"=>"Banner Title", "slug"=>"header-banner-title"},{ field_key: "editor", multiple: false, required: true})

        #content_manager = group_special.add_field_group({name: "Content", slug: "_group-special-content-manage", is_repeat: true })
        #content_manager.add_field({"name"=>"Content Image", "slug"=>"content-image"},{ field_key: "image", multiple: true})
        #content_manager.add_field({"name"=>"Content Details", "slug"=>"content-details"},{ field_key: "editor", multiple: true})

        map_banner_image = group_special.add_field_group({name: "Map Image", slug: "_group-special-map-image", is_repeat: false })
        map_banner_image.add_field({"name"=>"Map Image", "slug"=>"special-map-image"},{ field_key: "image", multiple: false})

        group_special_pick_up_spots = group_special.add_field_group({name: "Pick Up Spots", slug: "_group-related-pick-up-spots", is_repeat: false })
        group_special_pick_up_spots.add_field({"name"=>"Related Pick Up Spots", "slug"=>"related-pick-up-spots"},{ field_key: "posts", multiple: true  })
      end

      if !current_site.the_post_type("tourist-information").present?
        ti_data_options =  {has_category: false, has_tags: false, not_deleted: false, has_summary: false, has_content: false,
                            has_comments: false, has_picture: false, has_template: false, has_layout: false, has_seo: false}

        tourist_information = current_site.post_types.create(name: "Tourist Information", description:"Tourist Information Page", slug: "tourist-information", data_options: ti_data_options)

        group_information = tourist_information.add_field_group({name: "Tourist Information", slug: "_group-tourist-information", is_repeat: true})
        group_information.add_field({"name"=>"Schedule", "slug"=>"tourist-schedule"},{ field_key: "tourist_information_schedule", multiple: false })
      end

      #Pick Up Spots
      if !current_site.the_post_type("featured-list").present?
        pick_up_spots = current_site.post_types.create(name: "Featured", description:"For Article Pick up spots", slug: "featured-list", data_options: basic_data_options)
        #current_site.the_post_type("pick-up-spots")

        spots_header_banner_image = pick_up_spots.add_field_group({name: "Header Banner Image", slug: "_group-pick-up-spots-header-banner-image", is_repeat: false })
        spots_header_banner_image.add_field({"name"=>"Header Banner Image", "slug"=>"header-banner-image"},{ field_key: "image", multiple: false, required: true})
        spots_header_banner_image.add_field({"name"=>"Banner Title", "slug"=>"header-banner-title"},{ field_key: "editor", multiple: false, required: true})
        spots_header_banner_image.add_field({"name"=>"Sub Title", "slug"=>"sub-title"},{ field_key: "text_box", multiple: false, required: true})

        #content_manager = pick_up_spots.add_field_group({name: "Content", slug: "_group-pick-up-spots-content-manage", is_repeat: true })
        #content_manager.add_field({"name"=>"Content Image", "slug"=>"content-image"},{ field_key: "image", multiple: true})
        #content_manager.add_field({"name"=>"Content Details", "slug"=>"content-details"},{ field_key: "editor", multiple: true})

        basic_info = pick_up_spots.add_field_group({name: "Basic Information", slug: "_group-pick-up-spots-basic-info", is_repeat: false})
        basic_info.add_field({"name"=>"Basic Information", "slug"=>"pick-up-spots-basic-info"},{ field_key: "editor", multiple: false})

        train_access = pick_up_spots.add_field_group({name: "Train Access Pick Up Spots", slug: "_group-train-access-pick-up-spots", is_repeat: false})
        train_access.add_field({"name"=>"Train", "slug"=>"train-access-pick-up-spots"},{ field_key: "access_pick_up_spots", multiple: true })

        car_access = pick_up_spots.add_field_group({name: "Car Access Pick Up Spots", slug: "_group-car-access-pick-up-spots", is_repeat: false})
        airplane_access.add_field({"name"=>"Car", "slug"=>"car-access-pick-up-spots"},{ field_key: "access_pick_up_spots", multiple: true })

        bus_access = pick_up_spots.add_field_group({name: "Bus Access Pick Up Spots", slug: "_group-bus-access-pick-up-spots", is_repeat: false})
        bus_access.add_field({"name"=>"Bus", "slug"=>"bus-access-pick-up-spots"},{ field_key: "access_pick_up_spots", multiple: true })


        categories = [
                        {name: "Traditionals",        slug: "traditionals", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Nature",            slug: "nature", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Historical Sights", slug: "historical-sights", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Food & Drink",      slug: "food-and-drink", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Activity",          slug: "activity", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Shopping",          slug: "shopping", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Art & Museum",      slug: "art-and-museum", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Accommodation",      slug: "accommodation", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Spring",      slug: "spring", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Summer",      slug: "summer", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Autumn",      slug: "autumn", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                        {name: "Winter",      slug: "winter", parent_id: pick_up_spots.id, status: pick_up_spots.id},
                    ]

        ::Cama::Category.create(categories)

      end

      if current_site.the_post_type("featured-list").present? &&  !current_site.custom_field_groups.where(slug: "_group-area-information-pick-up-spots").present?
        pick_up_spots = current_site.the_post_type("featured-list")
        area_options = [
                          {title: nil, value: nil, default: true},
                          {title: "East", value: "east"},
                          {title: "Central", value: "central"},
                          {title: "West", value: "west"},
                        ]
        order_num_options = []
        10.times do |index|
          v = index == 0 ? nil : index
          order_num_options.push({title: v, value: v})
        end
        area_information = pick_up_spots.add_field_group({name: "Area Information", slug: "_group-area-information-pick-up-spots", is_repeat: false})
        area_information.add_field({"name"=>"Area", "slug"=>"area-pick-up-spots"},{ field_key: "select", multiple_options: area_options, multiple: false, required: false })
        area_information.add_field({"name"=>"Location Name", "slug"=>"location-name-pick-up-spots"},{ field_key: "text_box", multiple: false, required: false })
        area_information.add_field({"name"=>"Longitude", "slug"=>"longitude-pick-up-spots"},{ field_key: "text_box", multiple: false, required: false })
        area_information.add_field({"name"=>"Latitude", "slug"=>"latitude-pick-up-spots"},{ field_key: "text_box", multiple: false, required: false })
        area_information.add_field({"name"=>"Tag", "slug"=>"tag-pick-up-spots"},{ field_key: "text_box", multiple: false, required: false })
        area_information.add_field({"name"=>"Order Number", "slug"=>"order-number-pick-up-spots"},{ field_key: "select", multiple_options: order_num_options, multiple: false, required: false })
      end

      #end of pick up spots

      #list & Stories
      if !current_site.the_post_type("news-list").present?

        ls_options =  { has_category: true, has_tags: true, not_deleted: true, has_summary: false, has_content: true,
                        has_comments: false, has_picture: true, has_template: false, has_layout: false, has_seo: false }

        list_and_stories = current_site.post_types.create(name: "News & Topics ", description:"For Article List & Stories", slug: "news-list", data_options: ls_options)
        grp_stories = list_and_stories.add_field_group({name: "Header Banner Image", slug: "_group-list-and-stories-image", is_repeat: false })
        grp_stories.add_field({"name"=>"Header Banner Image", "slug"=>"header-banner-image"},{ field_key: "image", multiple: false, required: true})
        grp_stories.add_field({"name"=>"Banner Title", "slug"=>"header-banner-title"},{ field_key: "editor", multiple: false, required: true})
        grp_stories.add_field({"name"=>"Sub Title", "slug"=>"sub-title"},{ field_key: "text_box", multiple: false, required: true})

        categories = [
                        {name: "Traditionals",        slug: "traditionals", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Nature",            slug: "nature", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Historical Sights", slug: "historical-sights", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Food & Drink",      slug: "food-and-drink", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Activity",          slug: "activity", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Shopping",          slug: "shopping", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Art & Museum",      slug: "art-and-museum", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Accommodation",      slug: "accommodation", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Spring",      slug: "spring", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Summer",      slug: "summer", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Autumn",      slug: "autumn", parent_id: list_and_stories.id, status: list_and_stories.id},
                        {name: "Winter",      slug: "winter", parent_id: list_and_stories.id, status: list_and_stories.id},
                    ]

        ::Cama::Category.create(categories)
      end

      sanin_env = Rails.env == 'production' ? 'https://huber-japan.com/chat/tabishindan?type=san_in' : 'https://huber-stg.huber-japan.com/chat/tabishindan?type=san_in'

      # Header Main Menu
      if !current_site.nav_menus.find_by_slug('header-main-menu').present?
        menu = current_site.nav_menus.create(name: "Header Main Menu", slug: "header-main-menu")

        item1 = menu.append_menu_item({label: "Discover", type: "external", link: '#'})
        item1.append_menu_item({label: "About San`in", type: "external", link: '/about'})
        item1.append_menu_item({label: "Destinations", type: "external", link: '/destinations'})

        item2 = menu.append_menu_item({label: "Things to do", type: "external", link: '#'})
        item2.append_menu_item({label: "Featured Spots", type: "external", link: '/featured-list'})
        item2.append_menu_item({label: "News & Topics", type: "external", link: '/news-list'})

        item3 = menu.append_menu_item({label: "Plan Your Trip", type: "external", link: '#'})
        item3.append_menu_item({label: "Local Guides", type: "external", link: sanin_env, target: '_blank'})
        item3.append_menu_item({label: "For First-Timers", type: "external", link: '/first-timers'})
        item3.append_menu_item({label: "Accesss", type: "external", link: '/access'})
        item3.append_menu_item({label: "Accommodation", type: "external", link: '/accommodation'})
        item3.append_menu_item({label: "Tourist Information Center", type: "external", link: '/tourist-information'})
      end

      # Footer Main Menu
      if !current_site.nav_menus.find_by_slug('footer-menu-col1').present?
        menu = current_site.nav_menus.create(name: "Footer Menu Col1", slug: "footer-menu-col1")

        item1 = menu.append_menu_item({label: "Discover", type: "external", link: '#'})
        item1.append_menu_item({label: "About San`in", type: "external", link: '/about'})
        item1.append_menu_item({label: "Destinations", type: "external", link: '/destinations'})
      end


      # Footer Main Menu
      if !current_site.nav_menus.find_by_slug('footer-menu-col2').present?
        menu = current_site.nav_menus.create(name: "Footer Menu Col2", slug: "footer-menu-col2")

        item2 = menu.append_menu_item({label: "Things to do", type: "external", link: '#'})
        item2.append_menu_item({label: "Featured Spots", type: "external", link: '/featured-list'})
        item2.append_menu_item({label: "News & Topics", type: "external", link: '/news-list'})

        item3 = menu.append_menu_item({label: "Plan Your Trip", type: "external", link: '#'})
        item3.append_menu_item({label: "Local Guides", type: "external", link: sanin_env, target: '_blank'})
        item3.append_menu_item({label: "For First-Timers", type: "external", link: '/first-timers'})
        item3.append_menu_item({label: "Accesss", type: "external", link: '/access'})
        item3.append_menu_item({label: "Accommodation", type: "external", link: '/accommodation'})
        item3.append_menu_item({label: "Tourist Information Center", type: "external", link: '/tourist-information'})
        item3.append_menu_item({label: "Contact", type: "external", link: '/contact'})
      end

      value = {"fields":[{"field_type":"text","cid":"c1","label":"Name","required":"true","field_options":{"description":"","field_class":"input-field","template":"[ci]","field_attributes":"{\"onfocus\":\"this.value = '';\", \"onblur\":\"if (this.value == '') {this.value = '[label ci]...';}\"}"},"default_value":"[label ci]"},{"field_type":"text","cid":"c2","label":"Contact Number","required":"true","field_options":{"description":"","field_class":"input-field","template":"[ci]","field_attributes":"{\"onfocus\":\"this.value = '';\", \"onblur\":\"if (this.value == '') {this.value = '[label ci]...';}\"}"},"default_value":"[label ci]"},{"field_type":"email","cid":"c3","label":"Email","required":"true","field_options":{"description":"","field_class":"input-field","template":"[ci]","field_attributes":"{\"onfocus\":\"this.value = '';\", \"onblur\":\"if (this.value == '') {this.value = '[label ci]...';}\"}"},"default_value":"[label ci]"},{"field_type":"paragraph","cid":"c4","label":"Your Message","required":"true","field_options":{"description":"","field_class":"textarea-field","template":"[ci]","field_attributes":"{\"onfocus\":\"this.value = '';\", \"onblur\":\"if (this.value == '') {this.value = '[label ci]...';}\"}"},"default_value":"[label ci]"},{"field_type":"checkboxes","cid":"c6","label":"Checker","required":"true","field_options":{"description":"","options":[{"label":"I agree to my personal data being used by San’in Roots of Japan website to contact me."},{"label":"I have read the Personal Data and Cookies Policy and the Terms of Use."}],"field_class":"","template":"\u003cdiv class=\"form-checkbox\"\u003e\u003cinput class=\"checkbox\" name=\"agree\" type=\"checkbox\" value=\"I agree to my personal data being used by San’in Roots of Japan website to contact me.\"\u003e\u003clabel\u003eI agree to my personal data being used by San’in Roots of Japan website to contact me.\u003c/label\u003e\u003c/div\u003e\r\n\r\n\u003cdiv class=\"form-checkbox\"\u003e\u003cinput class=\"checkbox\" name=\"read\" type=\"checkbox\" value=\"I have read the Personal Data and Cookies Policy and the Terms of Use.\"\u003e\u003clabel\u003eI have read the \u003ca href=\"#\" target=\"_blank\"\u003ePersonal Data and Cookies Policy\u003c/a\u003e and the \u003ca href=\"#\" target=\"_blank\"\u003eTerms of Use.\u003c/a\u003e\u003c/label\u003e\u003c/div\u003e","field_attributes":""}},{"field_type":"submit","cid":"c5","label":"Submit","field_options":{"description":"","field_class":"contact-us-form-submit","template":"[ci]","field_attributes":""}}]}
      settings = {"railscf_mail"=>{"previous_html" =>"","after_html" =>"","to" =>"contact@mysite.com","subject" =>"New Contact Form","body" =>"","to_answer" =>"[c3]","subject_answer" =>"Contact form received","body_answer" =>"Hi [c1],\r\nThanks for contact us."}}
      current_site.contact_forms.create(name: 'Contact Form', slug: 'sanin-contact-form', value: value.to_json, settings: settings.to_json)

      #for_first_time_visitor_setup(theme)

      plugin_install('camaleon_post_order')
      theme.set_option('installed_at', Time.current.to_s)

  end

  def for_first_time_visitor_setup(theme)
    if !current_site.the_post_type("first-timers").present?

      ff_options =  { has_category: false, has_single_category: true, has_tags: true, not_deleted: true, has_summary: false, has_content: true,
                      has_comments: false, has_picture: true, has_template: false, has_layout: false, has_seo: false }

      ff_setup  = current_site.post_types.create(name: "For First-Time Visitor", description:"For first-time visitor", slug: "for-first-time-visitor", data_options: ff_options)
      grp_ff = ff_setup.add_field_group({name: "Header Banner Image", slug: "_group-for-first-time-visitor-image", is_repeat: false })
      grp_ff.add_field({"name"=>"Header Banner Image", "slug"=>"header-banner-image"},{ field_key: "image", multiple: false, required: true})
      grp_ff.add_field({"name"=>"Banner Title", "slug"=>"header-banner-title"},{ field_key: "editor", multiple: false, required: true})
      grp_ff.add_field({"name"=>"Sub Title", "slug"=>"sub-title"},{ field_key: "text_box", multiple: false, required: true})

      categories = [
                                          {name: "Spring",   slug: "spring", parent_id: ff_setup.id, status: ff_setup.id},
                                          {name: "Summer",   slug: "summer", parent_id: ff_setup.id, status: ff_setup.id},
                                          {name: "Autumn",   slug: "autumn", parent_id: ff_setup.id, status: ff_setup.id},
                                          {name: "Winter",   slug: "winter", parent_id: ff_setup.id, status: ff_setup.id},
                                        ]

      ::Cama::Category.create(categories)
    end
  end

  # callback executed after theme uninstalled
  def sanin_on_uninstall_theme(theme)
  end

  def custom_access_pick_up_spots(args)
    args[:fields][:access_pick_up_spots] = {
      key: 'access_pick_up_spots',
      label: 'Pick Up Spots',
      render: theme_view('custom_field/access_pick_up_spots.html.erb'),
      options: {
          required: true,
          multiple: true,
      },
      extra_fields:[
        {
            type: 'text_box',
            key: 'price',
            label: 'Price',
            description: 'price per trip'
        },
        {
            type: 'text_box',
            key: 'by_transpo_name',
            label: 'By Transportation Company Name',
            description: 'Company who offer the trip'
        },
        {
            type: 'text_box',
            key: 'duration',
            label: 'Duration',
            description: 'Approximate trip duration '
        },
      ]
    }
  end

  def custom_tourist_information_schedule(args)
    args[:fields][:tourist_information_schedule] = {
      key: 'tourist_information_schedule',
      label: ' Information Schedule',
      render: theme_view('custom_field/tourist_information_schedule.html.erb'),
      options: {
          required: true,
          multiple: true,
          translate: true
      },
      extra_fields:[
      ]
    }
  end

  def on_page_templating(args)

   post_type_slug =  args[:post_type].slug
   page_slug = params[:slug]

   case page_slug
     when 'discover'
       args[:render] = 'pages/discover' # put the name of your custom template
   end if post_type_slug == 'page'

  end

  def get_image_path(path)
      return theme_asset_url("images/#{path}")
  end

  def custom_contact_menu

    ctrl =  params[:controller]
    current_action = params[:action]
    check_contact_path =  "plugins/cama_contact_form/admin_forms#index"

    form = current_site.contact_forms.where(slug:"sanin-contact-form").first
    form_path = admin_plugins_cama_contact_form_admin_form_responses_path(form.id)

    if ctrl == "plugins/cama_contact_form/admin_forms"
     if (current_user.role != "admin" && ("#{ctrl}##{current_action}") == check_contact_path )
       redirect_to form_path
     end
    end

    admin_menu_add_menu('custom_last_menu', {icon: 'envelope-o', title: 'Inquiries', url: form_path })
  end
  
  def lazy_image_tag(source, options={})
    options['data-src'] = source
    
    if options[:class].blank?
      options[:class] = 'lazyload'
    else
      options[:class] = 'lazyload ' + options[:class]
    end

    return ActionController::Base.helpers.image_tag(get_image_path("dummy.gif"), options)
  end

  def get_lang
    I18n.locale.to_s
  end

  def locale_path path
    "/#{get_lang}#{path}"
  end

  def custom_draw_languages(list_class = "language-menu-list js-language-menu-list", current_page = false, current_class = "current_l", &block)
    lan = current_site.get_languages
    return  if  lan.size < 2
    res = ["<ul class='#{list_class}'>"]
    lan.each do |lang|
      path = lang.to_s+'.png'
      label =  (I18n.t "camaleon_cms.languages.#{lang.to_s}").html_safe
      #res << "<li class='language-menu-item #{ current_class if I18n.locale.to_s == lang.to_s}'> <a class='language-menu-link' href='#{cama_url_to_fixed(current_page ? "url_for" : "cama_root_url", {locale: lang, cama_set_language: lang})}'>#{label}</a> </li>"
      res << "<li class='language-menu-item #{ current_class if I18n.locale.to_s == lang.to_s}'> <a class='language-menu-link' href='#{cama_url_to_fixed(current_page ? "url_for" : "cama_root_url", {locale: lang})}'>#{label}</a> </li>"
    end
    res << "</ul>"
    res.join("").html_safe
  end

  def cama_url_to_fixed(url_to, *args)
    options = args.extract_options!
    current_site = options.delete(:site) || current_site
    if request.present?
      if options.include?(:locale) && options[:locale] == false
        options.delete(:locale)
      else
        options[:locale] = I18n.locale if !options[:locale].present? && current_site && current_site.get_languages.size > 1
      end
      options[:locale] = nil if options[:locale].present? && current_site && current_site.get_languages.first.to_s == options[:locale].to_s
    end

    options.delete(:format) if PluginRoutes.system_info["skip_format_url"].present?
    cama_current_site_host_port(options) unless options.keys.include?(:host)
    send(url_to.gsub('-', '_'), *(args << options))
  end


end
