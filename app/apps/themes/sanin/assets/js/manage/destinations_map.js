var DestinationsMap = DestinationsMap || {}
DestinationsMap.setting = {}

DestinationsMap.setting.loadMap = function(){

    var center = {lat: 35.7148, lng: 139.7967};
    var locations = gon.locations;

    // var locations = [
    //   ['Tokyo skytree',   35.7101, 139.8107],
    //   ['Tokyo Tower', 35.6586, 139.7454],
    //   ['Senso-ji', 35.7148, 139.7967],
    //   ['Sumida Aquarium', 35.7099, 139.8096]
    // ];

    var mapOptions = {
      mapTypeControlOptions: {
          mapTypeIds: ['Styled']
      },
      center: center,
      zoom: 13,
      disableDefaultUI: true,
      mapTypeId: 'Styled'
    };

    var mapStyles = [
      {
        featureType: 'water',
        elementType: 'geometry.fill',
        stylers: [
            { color: '#c1e4fd' }
          ]
      },
      {
        featureType: 'transit',
        elementType: 'labels',
        stylers: [
          { visibility: 'off' }
        ]
      },
      {
        featureType: 'poi.business',
        stylers: [
          { visibility: 'off' }
        ]
      }
    ];

    var mapContainer = document.getElementById('map');
    var map = new google.maps.Map(mapContainer, mapOptions);
    var mapStyle = new google.maps.StyledMapType(mapStyles, {name: 'Styled'});
    map.mapTypes.set('Styled', mapStyle);

    var infowindow =  new google.maps.InfoWindow({});

    var marker, count;
    var bounds = new google.maps.LatLngBounds();

    for (count = 0; count < locations.length; count++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[count][1], locations[count][2]),
          map: map,
          title: locations[count][0],
          label: (count + 1).toString()
        });

        google.maps.event.addListener(marker, 'click', (function (marker, count) {
          return function () {

            //infowindow.setContent(locations[count][0]);
            //infowindow.setContent(marker.label);
            //infowindow.open(map, marker);
            $('.carousel').slick('slickGoTo', (marker.label -1), true);
            map.panTo( new google.maps.LatLng( locations[count][1], locations[count][2] ) );
          }
        })(marker, count));
        bounds.extend(marker.getPosition());
      }

    // new google.maps.LatLng(center.lat, center.lng);
    map.fitBounds(bounds); //auto zoom
    DestinationsMap.setting.currentSpot( map );
}

DestinationsMap.setting.currentSpot = function( map ){

    $('.carousel').on('afterChange', function(event, slick, currentSlide, nextSlide){
      var currentSpots = $(".slick-current");
      var spotsDetails = currentSpots.find(".overview-carousel-inner");
      var lat =  spotsDetails.attr("data-lat");
      var lng =  spotsDetails.attr("data-lng");
      var locationName =  spotsDetails.attr("data-location-name");
      map.panTo( new google.maps.LatLng( lat, lng ) );
    
    });

}
