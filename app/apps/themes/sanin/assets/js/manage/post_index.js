var PostIndex = PostIndex || {}
PostIndex.setting = {}

PostIndex.setting.common = function(){

  $(".articleFilterBtn").on('click', function(e){
    $(".articleFilterContainer").toggleClass("open");
  });

  $(".js-contents-tag").on('click', function(e){
    category = $(this).text()
    category_id = $(this).attr('for')
    category_element = "cat-" + category_id
    if ($("." + category_element).length > 0) {
      $("." + category_element).remove()}
    else{
      $(".js-selected-categories").append("<p class=cat-" + category_id + ">" + category + "</p>");
    }
  });

}
