var HomeFirstView = HomeFirstView || {}
HomeFirstView.setting = {}

HomeFirstView.setting.loadhero = function(){
  if(!navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)){
    $(window).on('resize', function(){
      video_view_port()
    });
    $(window).load(function(){
       $(".wel-hero-video-thumb").css("display", "block")
       video_view_port()
      });
    $(document).ready(function(){
       $(".wel-hero-video-thumb").css("display", "block")
       video_view_port()
      });
  } else {
    $(window).load(function() {
      $('.wel-hero-slideshow').css('display', 'block');
      $('.wel-hero-slideshow li').css('opacity', '1');
      setInterval(slideSwitch, 3000);
    });
  }
  
  video_view_port = function (){
    video_height = $(window).width() / 16 * 9;
    $("#player").css("height", video_height);
  }
  
  slideSwitch = function() {
    var $active, $next;
    $active = $('.wel-hero-slideshow li.active');
    if ($active.length === 0) {
      $active = $('.wel-hero-slideshow li:last');
    }
    $next = $active.next().length ? $active.next() : $('.wel-hero-slideshow li:first');
    $active.addClass('last-active');
    $next.css({
      opacity: 0.0
    }).addClass('active').animate({
      opacity: 1.0
    }, 1000, function() {
      $active.removeClass('active last-active');
    });
  };
}