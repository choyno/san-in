// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.js
//= require camaleon_cms/bootstrap.min.js
//= require slick.js
//= require slick.min.js
//= require custom.js
//= require lazysizes.min.js
//= require lazysizes.unveilhooks.min.js

//= require ./sanin
//= require ./manage/post_index
//= require ./manage/destinations_map
//= require ./manage/home_first_view


$(function(){
  var app = new Sanin.App()
  app.start();
});

//= require_tree .



