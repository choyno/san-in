#!/bin/sh

# ALB の状態を確認する間隔（秒）
SLEEP=5

# リージョンを指定する

#######################################
# AWS CLI をチェックする
#######################################
check_awscli() {
  if ! which aws >/dev/null; then
    echo 'awscli not found'
    exit 2
  fi
}

#######################################
# ホスト名からインスタンス ID を取得する
# Arguments:
#   $1 HOST NAME
# Returns:
#   INSTANCE ID
#######################################
get_instance_id() {
  instance_id=$(/usr/bin/aws ec2 describe-instances --region ap-northeast-1 --filter "Name=tag:Name,Values=$1" --query Reservations[].Instances[].InstanceId --output text)
  if [ -z "$instance_id" ]; then
    echo 'host not found'
    exit 2
  fi
  echo ${instance_id}
}

#######################################
# インスタンスが ALB に反映されるまで待機する
# https://docs.aws.amazon.com/cli/latest/reference/elbv2/describe-target-health.html
# Arguments:
#   $1 TARGET GROUP ARN
#   $2 INSTANCE ID
#   $3 STATE (healthy or unused)
#######################################
alb_waiter() {
  while :
  do
    # 状態（healthy or unused）を取得する
    state=$(/usr/bin/aws elbv2 describe-target-health  --region ap-northeast-1 --target-group-arn $1 --targets Id=$2 --query TargetHealthDescriptions[].TargetHealth.State --output text)
    if [ "$3" = "${state}" ]; then
      echo "  TargetGroupARN: $1 InstanceId: $2 State:$3"
      break
    else
      echo "  TargetGroupARN: $1 InstanceId: $2 State:$state"
    fi
    sleep ${SLEEP}
  done
}

#######################################
# ALB からインスタンスを外す
# https://docs.aws.amazon.com/cli/latest/reference/elbv2/deregister-targets.html
# Arguments:
#   $1 TARGET GROUP ARN
#   $2 INSTANCE ID
#######################################
alb_deregister() {
  /usr/bin/aws elbv2 deregister-targets  --region ap-northeast-1 --target-group-arn $1 --targets Id=$2 > /dev/null
}

#######################################
# ALB にインスタンスを付ける
# https://docs.aws.amazon.com/cli/latest/reference/elbv2/register-targets.html
# Arguments:
#   $1 TARGET GROUP ARN
#   $2 INSTANCE ID
#######################################
alb_register() {
  /usr/bin/aws elbv2 register-targets  --region ap-northeast-1 --target-group-arn $1 --targets Id=$2 > /dev/null
}

