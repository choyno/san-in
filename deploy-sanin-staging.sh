#!/bin/sh

START_DATE_TIME=$(date)
echo "Deployment start! $START_DATE_TIME"

# スクリプトをインポートする
. ./aws_utils.sh

# コマンドチェックと定数の設定
check_awscli
# for sanin-staging
ALB_TARGET_GROUP_ARN='arn:aws:elasticloadbalancing:ap-northeast-1:022076518606:targetgroup/sanin-staging/9179ce2692a8bb44'

HOSTS=(sanin-staging01 sanin-staging02)

for host in ${HOSTS[@]}
do
  # インスタンス ID を取得する
  INSTANCE_ID=$(get_instance_id ${host})

  # unused になるまで待機
  echo " deregister : ${INSTANCE_ID}"
  alb_deregister ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID}
  alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'unused'
  #alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'draining'

  #
  # ホストごとにデプロイなどを実行する
  #
  echo " Deploy to $host !!"
  #if [ "${host}" = "sanin-staging01" ]; then
  #  ROLES=sanin-staging01 bundle exec cap staging deploy
  #elif [ "${host}" = "sanin-staging02" ]; then
  #  ROLES=sanin-staging02 bundle exec cap staging deploy
  #fi
  ROLES=${host} bundle exec cap staging deploy

  # healthy になるまで待機
  echo " register : ${INSTANCE_ID}"
  alb_register ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID}
  alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'healthy'
done

END_DATE_TIME=$(date)
echo "Deployment end! $END_DATE_TIME"

DIFF_TIME=$(expr `date -d"$END_DATE_TIME" +%s` - `date -d"$START_DATE_TIME" +%s`)

echo "Deployment time: $DIFF_TIME sec"
