namespace :tourist_information do

  desc "Populate Tourist Information"

  task :populate => :environment do

    site = Cama::Site.where(name: "sanin").first.decorate
    site.the_posts('tourist-information').delete_all
    tourist_information = site.post_types.where(slug:"tourist-information").first
    tourist_schedule =  CamaleonCms::CustomField.where(slug: "tourist-schedule").first

    tourist_information_datas.each do |data|

      puts "Creating #{data[:airport]}"
      puts " Count #{data[:schedules].size}"


      schedule_hldr = {}

      data[:schedules].each_with_index do |sched, idx|

        content_1 = get_post_detail(site, sched[:related_contents][0][:link_slug])
        content_2 = get_post_detail(site, sched[:related_contents][1][:link_slug])

        generate_sched = { "#{idx}": {
            "tourist-schedule": {
              "id": tourist_schedule.id,
              "group_number": "#{idx}",
              "values": {
                "#{idx}": {
                  "itc": sched[:ITC],
                  "address": sched[:address],
                  "tel": sched[:tel],
                  "time": sched[:time],
                  "mail": sched[:mail],
                  "web_address": sched[:web],
                  "language": sched[:languages],
                  "wifi": ("◯" if sched[:wifi]),
                  "internet": ("Available (Free)" if sched[:internet_pc]),
                  "latitude": sched[:latitude],
                  "longitude": sched[:longitude],
                  "content_1": content_1[:id],
                  "content_2": content_2[:id]
                }
              }
            }
          }
        }
        schedule_hldr = schedule_hldr.merge!(generate_sched)
      end

      params = {
        "post": {
          "draft_id": "",
          "slug": data[:slug],
          "title": data[:airport],
          "status": "published"
        },
        "meta": {
          "slug": data[:slug]
        },
        "field_options":  schedule_hldr,
        "post_type_id": tourist_information.id,
        "user_id": Cama::User.first.id,
        "status": "published",
        "data_tags": [],
        "data_categories": []
      }

      post_data = get_post_data(params, true)

      @post = tourist_information.posts.new(post_data)
      r = {post: @post, post_type: tourist_information};
       @post = r[:post]
      if @post.save
        @post.set_metas(params[:meta])
        @post.set_field_values(params[:field_options])
        @post.set_options(params[:options])
      end
    end
  end


  def get_post_data(params, is_create = false)
    post_data = params[:post]
    post_data[:user_id] = Cama::User.first.id
    post_data[:status] = 'published'
    post_data[:data_tags] = params[:tags].to_s
    post_data[:data_categories] = params[:categories] || []
    post_data
  end

  private

  def tourist_information_datas
    [
      {"airport": "Tottori Sand Dunes Conan Airport", slug:"tottori-sand-dunes-conan-airport",
       schedules: [
         {
           ITC: "Tottori City International Tourist Support Center",  latitude: "35.4931918", longitude: "134.2257536", address:"117 Higashihonjicho Town, Tottori City, Tottori Pref. 680-0835", tel: "0857-22-7935", time: "8:30 AM - 5:30 PM (All year exept 31th December and 1st January)", mail:"international@hal.ne.jp", web: "https://www.city.tottori.lg.jp/www/contents/1336986251261/index_k.html", web_JNTO: "https://tic.jnto.go.jp/detail.php?id=1246",
           languages: "English(Full-Time, Chinese(Part-Time), Korean(Part-Time)", wifi: true, internet_pc: "Available (Free)",
           related_contents: [{link_slug: "tottori-dune" }, {link_slug: "ishitan-residence" }]
         },
         {
           ITC: "Kurayoshi Shirakabe-dozo Tourist Information Center",   latitude:"35.3806919", longitude:"133.1923825", address:"2568 Uomachi Town, Kurayoshi City, Tottori Pref. 682-0821 (in Akagawara-jyugokan)", tel:"0858-22-1200", time:"8:30 AM - 5:00 PM (10:00 AM - 4:00 PM from 31 Dec. to 3 Jan.)", mail:"info@kurayoshi-kanou.jp", web:"https://www.kurayoshi-kankou.jp/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3321",
           languages: "English(Full-Time),Chinese(Full-Time), Korean(Full-Time)", wifi: true, internet_pc: false,
           related_contents: [{link_slug: "aka-gawara" }, {link_slug: "east-onsen" }]
         },
         {
           ITC: "Central Tottori International Tourist Support Center", latitude:"35.4543843",  longitude:"133.8467897", address:"2F 2-1-2 Ageicho Town, Kurayoshi City, Tottori Pref. 682-0022", tel:"0858-24-5024", time:"8:30 AM - 5:30 PM (except 31 December and 1 January)", mail:"", web:"https://tottori-iyashitabi.com/en/profile/476/", web_JNTO:"",
           languages: "", wifi:false, internet_pc: false,
           related_contents: [{link_slug: "mitokusan" }, {link_slug: "east-onsen" }]
         }
        ]
      },

      {"airport": "Yonago Kitaro Airport ", slug: "yonago-kitaro-airport",
       schedules: [
         {
           ITC: "Sakaiminato City Tourist Information Center", latitude:"35.5453704",  longitude:"133.2206009", address:"215 Taishomachi Town, Sakaiminato City, Tottori Pref. 684-0004", tel:"0859-47-0121", time:"9:00 AM - 4:00 PM Mar. - Oct. 9:00 AM - 5:00 PM Nor. - Feb.", mail:"info@sakaiminato.net", web:"http://www.sakaiminato.net/foreign/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1248",
           languages: "English(Full-Time),Chinese(Full-Time), Korean(Part-Time)", wifi: true, internet_pc: false,
           related_contents: [{link_slug: "okinoshima-1" }, {link_slug: "central-onsen" }]
         },
         {
           ITC: "Yonago Airport General Information", latitude:"35.5006141",  longitude:"133.2429002", address:"1634 Sainokamicho Town, Sakaiminato City, Tottori Pref. 684-0055", tel:"0859-45-6123", time:"8:00 AM - Last arrival flights.", mail:"contact@yonago-air.com", web:"http://www.yonago-air.com/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3213",
           languages: "English(Full-Time), Chinese(Part-Time), Korean(Part-Time)", wifi:true, internet_pc:false,
           related_contents: [{link_slug: "miho-shrine" }, {link_slug: "daisen" }]
         },
         {
           ITC: "Adachi Museum of Art International Tourist Information Desk",  latitude:"35.3806919", longitude:"133.1923825", address:"320 Furukawacho Town, Yasugi City, Shimane Pref. 692-0064", tel:"0854-28-7111", time:"9:00 AM - 5:30 PM Apr. - Sep. 9:00 AM - 5:00 PM Oct. - Mar. (Open year round)", mail:"info@adachi-museum.or.jp", web:"http://www.adachi-museum.or.jp/en/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1250",
           languages: "English(Full-Time), Chinese(Part-Time)", wifi:true, internet_pc:false,
           related_contents: [{link_slug: "adachi-art-museum" }, {link_slug: "central-onsen" }]
         },
         {
           ITC: "Yasugi-city Tourist Information Center",  latitude:"35.4282359", longitude:"133.26", address:"2093-3 Yasugicho Town, Yasugi City, Shimane Prefecture 692-0011", tel:"0854-23-7667", time:"7:30 AM - 7:00 PM (All year exept 31th December and 1st January)", mail:"mail@yasugi-kankou.jp", web:"http://www.yasugi-kankou.com/eng/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3297",
           languages: "English(Full-Time)", wifi:false, internet_pc:false,
           related_contents: [{link_slug: "adachi-art-museum" }, {link_slug: "central-onsen" }]
          }
        ]
      },

      {"airport": "Izumo Enmusubi Airport", slug:"izumo-enmusubi-airport",
       schedules: [
         {
           ITC: "Izumo Airport Information Counter",  latitude:"35.414536", longitude:"132.8835177", address:"2633-1 Okinosu, Hikawacho Town, Izumo City, Shimane Pref. 699-0551", tel:"0853-72-7500", time:"8:00 AM - 8:30 PM", mail:"", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3245",
           languages: "English(Full-Time)", wifi:true, internet_pc:false,
           related_contents: [{link_slug: "central-onsen" }, {link_slug: "iwami-ginzan" }]
         },
         {
           ITC: "Izumoshi Station Tourist Information Center",  latitude:"35.3607042", longitude:"132.7542431", address:"11 Ekikitamachi Town, Izumo City, Shimane Pref. 693-0007 (in JR Izumoshi Station)", tel:"0853-30-6015", time:"8:30 AM - 5:00 PM", mail:"", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3235",
           languages: "English(Full-Time)", wifi:false, internet_pc: false ,
           related_contents: [{link_slug: "wako-hakubutsukan" }, {link_slug: "iwami-ginzan" }]
         },
         {
           ITC: "Shinmondori Visitor Center",  latitude:"35.3948268", longitude:"132.6846344", address:"780-4 kizukiminami, Taishamachi Town, Masuda City, Shimane Pref. 699-0711", tel:"0853-53-2298", time:"9:00 AM - 5:00 PM", mail:"taisha@kankou-taisha.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3168",
           languages: "English(Full-Time)", wifi: true, internet_pc: false,
           related_contents: [{link_slug: "izumotaisha" }, {link_slug: "central-onsen" }]
         },
         {
           ITC: "Matsue International Tourist Information Center",  latitude:"35.4643147", longitude:"133.0612993", address:"665 Asahimachi Town, Matsue City, Shimane Pref. 690-0003 (in JR Matsue Station)", tel:"0852-21-4034", time:"9:00 AM - 6:00 PM (9:00 AM - 7:00 PM Jun.-Oct. Open year round)", mail:"info@kankou-matsue.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1249",
           languages: "English(Full-Time), Chinese(Part-Time), Korean(Part-Time)", wifi:true, internet_pc:true,
           related_contents: [{link_slug: "matsue-castle" }, {link_slug: "ichibata-dera" }]
         },
         {
           ITC: "Matsue Urban Hotel",  latitude:"35.4653257", longitude:"133.0625463", address:"590-3 Asahimachi Town, Matsue City, Shimane Pref. 690-0003", tel:"0852-22-0002", time:"24H", mail:"", web:"http://station.matsue-urban.co.jp/en/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3221",
           languages: "English(Full-Time), Chinese(Part-Time)", wifi: true, internet_pc: true,
           related_contents: [{link_slug: "matsue-castle" }, {link_slug: "sake" }]
         },
         {
           ITC: "Matsue New Urban Hotel",  latitude:"35.4686999", longitude:"133.0484225", address:"40-1 Nishichamachi Town, Matsue City, Shimane Pref. 690-0845", tel:"0852-23-0003", time:"24H", mail:"new-urban@matsue-urban.co.jp", web:"http://new.matsue-urban.co.jp/en", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=3217",
           languages: "English(Full-Time)", wifi: true, internet_pc: true,
           related_contents: [{link_slug: "matsue-castle" }, {link_slug: "central-onsen" }]
         }
        ]
      },

      {"airport": "Hagi Iwami Airport", slug: "hagi-iwami-airport",
       schedules: [
         {
           ITC: "Masuda City Tourist Information Center (Masuda Tourist Association)",  latitude:"34.6775907", longitude:"131.8374349", address:"17-2 Ekimaecho Town, Masuda City, Shimane Pref. 698-0024", tel:"0856-22-7120", time:"9:00 AM - 5:30 PM (All year exept 29th December and 3rd January)", mail:"info2@masudashi.com", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1406",
           languages: "English(Full-Time)",  wifi: true, internet_pc: true,
           related_contents: [{link_slug: "iwami-kagura" }, {link_slug: "west-onsen-1" }]
         },
         {
           ITC: "Tsuwano International Information Center (Tsuwano Tourism Association)",  latitude:"34.4724926", longitude:"131.7717818", address:"I71-2 Ushiroda, Tsuwano Town, Kanoashi District, Shimane Pref. 699-5605", tel:"0856-72-1771", time:"9:00 AM - 5:00 PM", mail:"tsuwanok@tsuwano.net", web:"http://www.tsuwano.ne.jp/kanko/", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1405",
           languages: "English(Full-Time)", wifi: true, internet_pc: false,
           related_contents: [{link_slug: "tonomachi" }, {link_slug: "taiko-dani" }]
         },
         {
           ITC: "Hagi Hotel & Restaurant Takadai",  latitude:"34.4103198", longitude:"131.400024", address:"80 Karahimachi Town, Hagi City, Yamaguchi Pref. 758-0044", tel:"0838-22-0065", time:"9:00 AM - 9:00 PM", mail:"takadai@takadai.co.jp", web:"", web_JNTO:"https://tic.jnto.go.jp/detail.php?id=1266",
           languages: "English(Full-Time), Chinese(Full-Time)", wifi: true, internet_pc: true,
           related_contents: [{link_slug: "taiko-dani" }, {link_slug: "hagi" }]
         },
         {
           ITC: "Hagi City Tourism Assciation",  latitude:"34.3932368", longitude:"131.3818331", address:"3537-3 Tsubaki, Hagi City, Yamaguchi Pref. 758-0061", tel:"0838-25-1750", time:"9:00 AM - 5:45 PM Mar. - Nov. 9:00 AM - 5:00 PM Dec. - Feb.", mail:"info@hagishi.com", web:"http://www.hagishi.com/en/", web_JNTO:"http://www.hagishi.com/en/",
           languages: "English(Full-Time)", wifi: true, internet_pc: false,
           related_contents: [{link_slug: "taiko-dani" }, {link_slug: "hagi" }]
         }
        ]
      }
    ]
  end

  def get_post_detail(site, slug)
    post = { name: "", url: "", image:"", id:  "" }
    get_post =  site.the_posts('featured-list').where(slug: slug).first
    if get_post.present?
      dget_post = get_post.decorate
      img_path = dget_post.the_fields('header-banner-image')[0]
      banner_title = dget_post.the_fields('banner-title')[0]
      name =  banner_title.present? ? banner_title : dget_post.title

      url =  dget_post.slug
      post = { name: name, url: "/featured/#{url}", image: img_path, id: dget_post.id }
    end

    return post
  end


end
