#!/bin/sh

START_DATE_TIME=$(date)
echo "Deployment start! $START_DATE_TIME"

# スクリプトをインポートする
. ./aws_utils.sh

# コマンドチェックと定数の設定
check_awscli
# for sanin-production
ALB_TARGET_GROUP_ARN='arn:aws:elasticloadbalancing:ap-northeast-1:022076518606:targetgroup/sanin-production/123da9178c95b4de'

HOSTS=(sanin-production01 sanin-production02)

for host in ${HOSTS[@]}
do
  # インスタンス ID を取得する
  INSTANCE_ID=$(get_instance_id ${host})

  # unused になるまで待機
  echo " deregister : ${INSTANCE_ID}"
  alb_deregister ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID}
  alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'unused'
  #alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'draining'

  #
  # ホストごとにデプロイなどを実行する
  #
  echo " Deploy to $host !!"
  #if [ "${host}" = "sanin-production01" ]; then
  #  ROLES=sanin-production01 bundle exec cap staging deploy
  #elif [ "${host}" = "sanin-production3" ]; then
  #  ROLES=sanin-production03 bundle exec cap staging deploy
  #fi
  ROLES=${host} bundle exec cap production deploy

  # healthy になるまで待機
  echo " register : ${INSTANCE_ID}"
  alb_register ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID}
  alb_waiter ${ALB_TARGET_GROUP_ARN} ${INSTANCE_ID} 'healthy'
done

END_DATE_TIME=$(date)
echo "Deployment end! $END_DATE_TIME"

DIFF_TIME=$(expr `date -d"$END_DATE_TIME" +%s` - `date -d"$START_DATE_TIME" +%s`)

echo "Deployment time: $DIFF_TIME sec"
