# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "Sanin"
set :repo_url, "git@github.com:huberinc/san-in.git"

set :rbenv_custom_path, '/home/ec2-user/.rbenv'
set :rbenv_ruby, '2.5.1'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :stage,  :production
set :branch, :master

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"
set :deploy_to, "/home/ec2-user/sanin_app"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true
set :pty, true
set :log_level, :debug

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"
#set :linked_files, %w{ .env }

set :linked_files, fetch(:linked_files, []).push(
  '.env',
  'config/master.key'
)

# Default value for linked_dirs is []
#set :linked_dirs, "tmp/cache"
set :linked_dirs, fetch(:linked_dirs, []).push(
  'log',
  'tmp/pids',
  'tmp/cache',
  'tmp/sockets',
  'public/uploads'
)

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5
set :keep_releases, 3

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

set :assets_roles, [:web, :app]
set :user, 'ec2-user'

set :puma_user, fetch(:user)
set :puma_role, :app
set :puma_threads,    [5, 16]
set :puma_workers,    2
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/puma.sock"
#set :puma_bind,       "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{shared_path}/log/puma.access.log"
set :puma_error_log,  "#{shared_path}/log/puma.error.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true

#module SSHKit
#  class Coordinator
#    private
#      def default_options
#        { in: :groups, limit: 1 }
#      end
#  end
#end

#set :whenever_command, "bundle exec whenever --update-crontab"
#set :whenever_environment, fetch(:rails_env)
set :whenever_identifier, "#{fetch(:application)}_#{fetch(:rails_env)}"
set :whenever_roles, :batch

namespace :puma do

  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
      execute "mkdir #{shared_path}/tmp/cache -p"
    end
  end

  desc 'Puma restart for Sanin service'
  task :sanin_restart do
    invoke! 'puma:stop'
    invoke! 'puma:start'
  end

  desc 'Puma restart for Sanin service'
  task :sanin_restart_shell do
    on roles(:app) do
      execute "sudo /etc/init.d/puma restart"
    end
  end

  # for tempolaly
  #Rake::Task["puma:start"].clear_actions
  #Rake::Task["puma:stop"].clear_actions
  #Rake::Task["puma:restart"].clear_actions

  #before :start, :make_dirs
  #after :start, :restart
end

namespace :slack do

  #Rake::Task["slack:notify_finished"].clear_actions

  #before :start, :make_dirs
  #after 'puma:start', 'slack:notify_finished'
end

namespace :deploy do
  desc 'Create files and directorys'
  task :createfiles do
    on roles(:app, :batch) do |host|
      if test("[ ! -d #{release_path}/log ]")
        execute("mkdir -p #{release_path}/log")
      end
    end
  end

  desc 'Upload .env'
  task :upload_dotenv do
    on roles(:app, :batch) do |host|
      upload! ".env_#{fetch :rails_env}", "#{shared_path}/.env"
    end
  end

 desc 'Upload master.key'
 task :upload_masterkey do
   on roles(:app, :batch) do |host|
     upload! "config/master.key", "#{shared_path}/config/master.key"
   end
 end

  desc 'execute the seed migration'
  task :seed do
    on roles(:db) do
      with rails_env: fetch(:rails_env) do
        within release_path do
          execute :bundle, :exec, :rake, 'db:'
        end
      end
    end
  end

  desc 'execute the whenever updating'
  task :whenever_update do
    on roles(:batch) do
      with rails_env: fetch(:rails_env) do
        within release_path do
          #execute :bundle, :exec, 'whenever --update-crontab'
          execute 'sudo /etc/init.d/crond restart'
        end
      end
    end
  end

  desc 'Original task for finalize deployment'
  task :finish_deploy do
    on roles(:app, :batch) do
      with rails_env: fetch(:rails_env) do

      end
    end
  end


  Rake::Task["delayed_job:restart"].clear_actions
  Rake::Task["puma:smart_restart"].clear_actions
  Rake::Task["puma:restart"].clear_actions
  before 'deploy:check:linked_files', :upload_dotenv
  before 'deploy:check:linked_files', :upload_masterkey
  after 'deploy:updating', 'puma:stop'
  after 'deploy:updating', :createfiles
  after 'deploy:log_revision', :whenever_update
  after 'deploy:log_revision', 'puma:sanin_restart'

end


namespace :assets do
  desc 'Precompile assets locally and then rsync to web servers'
  task :precompile do
    on roles(:app) do
      rsync_host = host.to_s
      run_locally do
        with rails_env: fetch(:rails_env) do
          execute 'rake assets:precompile'
        end

        #execute "rsync -raz public/assets #{fetch(:user)}@#{rsync_host}:#{shared_path}/public/"
        execute "rsync -raz public/assets #{fetch(:user)}@#{rsync_host}:#{release_path}/public/"
        #execute 'rm -rf public/assets'
      end
    end
  end
end

# for Slack Notification
set :slack_url, 'https://hooks.slack.com/services/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'
set :slack_channel, '#deploy'
set :slack_emoji, ':tomo:'
set :slack_username, 'TOMO(SANIN) < Start deployment! Go Go Go!!'

before 'slack:notify_finished', :deploy_success do
  set :slack_emoji,    ':tomo:'
  set :slack_username, 'TOMO(SANIN) < Successful deployment! Yippee!! '
end

before 'slack:notify_failed', :deploy_failure do
  set :slack_emoji,    ':tomo:'
  set :slack_username, 'TOMO(SANIN) < OMG!! Deployment failure!!'
end

