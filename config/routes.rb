Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #root 'camaleon_cms/frontend#index'
  
  scope "(:locale)", locale: /#{PluginRoutes.all_locales}/, :defaults => {  } do
    root 'themes/sanin/public#home'
  end

  resources :healthcheck, only: [:index]

end
