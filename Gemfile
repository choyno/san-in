source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
gem 'pg', '~> 1.1', '>= 1.1.3'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
gem 'sassc-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 3.5.5'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby
gem 'slim-rails', '~> 3.1.3'

gem 'jquery-rails', '~> 4.3', '>= 4.3.3'

#use dotenv for .env
gem 'dotenv-rails', '~> 2.5.0'

#use seed_fu for import seed data
gem 'seed-fu', '~> 2.3'

gem 'json', '2.2.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

gem 'google-analytics-rails', '~> 1.1.1'

gem 'rails-i18n', '~> 5.1', '>= 5.1.2'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# AWS SDK
gem 'aws-sdk', '~>2'

# cron management
gem 'whenever', '0.9.4', :require => false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Use Capistrano for deployment
  gem 'capistrano', '~> 3.5', require: false
  gem 'capistrano-ext', require: false
  gem 'capistrano_colors', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma', require: false
  gem 'capistrano-rbenv', require: false
  gem 'capistrano-delayed-job', '~> 1.0', require: false
  gem "capistrano-slackify", require: false
  gem "capistrano-locally", require: false
end

# Slack
#gem 'slack-notifier'

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

#content manager
#gem 'camaleon_cms', git: 'https://github.com/marlo-morales/camaleon-cms.git', branch: :master

#gem 'camaleon_cms', path: '~/Desktop/workspace/camaleon-cms'
gem 'camaleon_cms', git: 'https://github.com/owen2345/camaleon-cms.git', tag: :"2.4.5.12"

gem 'camaleon_post_order', github: 'owen2345/camaleon-post-order-plugin'
#gem 'cama_external_menu', github: 'owen2345/cama_external_menu'
#gem 'cama_language_editor'
gem 'draper', '~> 3'


#################### Camaleon CMS include all gems for plugins and themes ####################
require './lib/plugin_routes'
instance_eval(PluginRoutes.draw_gems)

gem 'facebook_sn', path: 'apps/plugins/facebook_sn'
